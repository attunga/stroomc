stroomc - Log Collection Agent for Stroom
===========================

# Overview
stroomc is a Linux log collection agent for Stroom used to collect audit logs from a Linux based endpoint and forward these to a Stroom server.

Currently this application supports Red Hat, CentOS and other derivatives. It is also most likely possible to run the application   

Apart from the ability to upload audit logs to a Stroom servers,  the application is fully configurable from a single YAML configuration file.   The configuration file covers a number of areas such as:

* Feed Information including collection of local system information. 
* Disk locations including queue, logging and temporary locations.
* Logging information including controlling logging levels, logging location whether this be to console or to disk.
* More here
 
# Installation

Installation is best done though a package which will complete the following tasks that can also be done manually:

* Sets the application installation location.
* Installs the configuration file to /etc/stroomc/stroomc.yml  The configuration file can also be run from the same directy as the application file.
* Creates a service to automatically run the application.
* Creates the logging directory as specified in the configuration file.
* Creates the log rotation file to help rotate logs.

# Concepts

The basic concept of this application is that it runs seamlessly from a service in the background taking processed copies of audit logs, processing them via ausearch,  queuing them to disk and then uploading them to a stroom server. 

The only command line usage is to get the agent info.   This can be done as follows using root and will show the currently detected feed and application info.

```
# stroomc info
```

# Configuration 

Configuration is controlled from the /etc/stroomc/stroomc.yml configuration file with comments on most configuration options.

In most cases you would consider changing the following options. 

* <b>System Identification and Feed Settings:</b>  Sets any custom required feed information.
* <b>Stroom Proxies:</b> The location of the local Stroom Proxies.
* <b>IgnoreTLSCertificateErrors:</b>  Whether you need to ignore unsigned or invalid certificates on the Stroom Proxy.  Ignoring invalid certificates is insecure but can be useful when connecting to a development server.
* <b>QueueDirectory:</b> The Stroom Queue directory,  in most cases this will not need to be changed.
* <b>Audit Log Related Settings:</b> In most cases these will not change  unless the audit log file is in a non standard location.
* <b>LogProcessingFrequency:</b> This is set by default at 10 minutes, it can be set to less for a busy system or when debugging the application.
* <b>LogLevel:</b> The log level is at Info as a default which will log uploads but can be set to various levels depending on the information required in the logs.

# Changelog
* v0.2.0 15 August 2021: Initial release