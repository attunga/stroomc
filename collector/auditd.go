/*
Copyright © 2021 Lindsay Steele <lgsteele@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package collector

import (
	"compress/gzip"
	log "github.com/sirupsen/logrus"
	"gitlab.com/attunga/stroomc/settings"
	"gitlab.com/attunga/stroomc/utils"
	"os"
	"os/exec"
	"strconv"
	"time"
)

// ProcessAuditDLogs - Does the collection of the audit logs.
//Setting should be trusted before getting to here.
func ProcessAuditDLogs(settings *settings.AuditdSettings) (filesCopied int, err error) {

	log.Debugln("Entered Process Audit Logs: ", settings.QueueDirectory)

	//  First thing to do is work out whether we need to insert node names due to bad configuration
	//  This could also come from a configuration setting to override this and make things more efficient.
	// It could also be something that is done on startup once and not something that happens on every single pass

	log.Debug("Need to Insert Node Names: ", settings.NeedNodeNames)

	// By now we should know our node file is OK

	// Look for a checkpoint file,  if we have one then we use it otherwise we create a new checkpoint

	var processorArgs []string

	processorArgs = append(processorArgs, "-i", "-if", settings.AuditLogFile)
	processorArgs = append(processorArgs, "--checkpoint", settings.CheckpointFile)

	// If we have a checkpoint file,  we just log what we are using
	if utils.FileExistsSimple(settings.CheckpointFile) {
		log.Debug("Using existing ausearch checkingpoint file: ", settings.CheckpointFile)
	} else {
		// The checkpoint file does not exist so we will start a new checkppoint file by adding a parameter.
		processorArgs = append(processorArgs, "--start", "checkpoint")
		log.Debug("Ausearch Checkpoint file not found, starting new checkpoint file: ", settings.CheckpointFile)
	}

	log.Debug("Args to use: ", processorArgs)

	//	if [ -n "${AuditLogFile}" ]; then
	//	ProcessorArgs="-i -if ${AuditLogFile} --checkpoint ${STROOM_CHECKPOINT_FILE}"
	//	else
	//  TODO:  Try to understand what the --input-logs part mean,  is then when files are passed as a parameter????
	//	ProcessorArgs="-i --input-logs --checkpoint ${STROOM_CHECKPOINT_FILE}"
	//	fi
	//	ProcessorRecoverArgs="${ProcessorArgs} --start checkpoint"

	// Process the auditd logs using ausearch
	// This function
	// - ensures all logs have a node=<hostname> key-value pair on every line in case auditd.conf is misconfigured/overwritten
	// - we set LC_TIME="en_DK" to get an ISO8601 format YYYY-MM-DD hh:mm:ss.msec
	// - we set TZ=UTC to ensure the date/time is consistent - ie UTC timezone as there
	//   is no timezone in the ausearch date timestamp.

	errenv1 := os.Setenv("LC_TIME", "en_DK")
	if errenv1 != nil {
		log.Fatalf("err %v", errenv1)
	}

	errenv2 := os.Setenv("TZ", "UTC") // TODO: Make this error message better
	if errenv2 != nil {
		log.Fatalf("err %v", errenv2) // TODO: Make this error message better
	}

	// Here we run the ausearch commands and get the entries back as a string
	// We might not need the temp location after all - it seemed to be a logging thing.
	cmd := exec.Command(settings.AuditDProcessor, processorArgs...)
	collectedAuditEntries, err := cmd.Output()

	// Work out why ausearch did not work
	if err != nil {
		if err.Error() == "exit status 1" && len(collectedAuditEntries) == 0 {
			log.Debug("Error getting any audit logs,  most likely there were none available")
		} else {
			log.Error("Error Processing Auditd Log: ", err.Error())
		}
	}

	if len(collectedAuditEntries) > 0 {
		// Write out the file with the best speed possible
		outputFileName := settings.QueueDirectory + "/StroomAuditDLogFile." + strconv.FormatInt(time.Now().Unix(), 10) + ".gz"
		log.Debug("USing Temporary Output Filename: ", outputFileName)

		// Write with BestSpeed.
		outputGZipFile, _ := os.Create(outputFileName)
		w, err := gzip.NewWriterLevel(outputGZipFile, gzip.BestSpeed)
		w.Write([]byte(collectedAuditEntries))
		w.Close()

		// Log if we had issues writing the file to disk
		if err != nil {
			log.Error("Could not write out log file ", outputGZipFile, " to disk. Error: ", err.Error())
		}
		filesCopied = 1 // TODO: This is kind o fsilly to count files when there will only be one,  refactor in future.

		// Now we have written audit files to disk,  write the logs
		readyFile, err := os.Create(outputFileName + ".ready")
		if err != nil {
			log.Error("Could not create the ready file for: ", readyFile, " Error: ", err.Error())
		}
		readyFile.Close()

	}

	// TODO: Go over this .. do we really need to send the errors back up or should they be dealt with here.
	return filesCopied, nil
}
