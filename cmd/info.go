/*
Copyright © 2021 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/attunga/stroomc/settings"
)

// infoCmd represents the info command
var infoCmd = &cobra.Command{
	Use:   "info",
	Short: "A brief description of your command",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Run: func(cmd *cobra.Command, args []string) {
		log.Debug("info called")

		hostinfo := settings.FeedInfo{}.LoadFeedInfo()
		fmt.Println("Program Version: ", settings.Version)
		fmt.Println("Build Date: ", settings.BuildTime)
		fmt.Println("Hostname: ", hostinfo.Hostname)
		fmt.Println("IP: ", hostinfo.IpAddress)
		fmt.Println("TimeZone: ", hostinfo.TimeZone)
		fmt.Println("Feed: ", hostinfo.FeedName)     // unique Feed name from configuration
		fmt.Println("System: ", hostinfo.SystemType) // Server or workstation
		fmt.Println("Environment: ", hostinfo.Environment)
		fmt.Println("Feed Version: ", hostinfo.FeedVersion)
		fmt.Println("MyHost: ", hostinfo.Hostname)
		fmt.Println("MySecurityDomain: ", hostinfo.MySecurityZone)

	},
}

func init() {
	rootCmd.AddCommand(infoCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// infoCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// infoCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
