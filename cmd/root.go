/*
Copyright © 2021 Lindsay Steele <lgsteele@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package cmd

import (
	"fmt"
	"github.com/mitchellh/go-homedir"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/attunga/stroomc/utils"
	"os"
	"strings"
)

var cfgFile string
var configDirective string

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "stroomc",
	Short: "A brief description of your application",
	Long: `A longer description that spans multiple lines and likely contains
examples and usage of using your application. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	// Uncomment the following line if your bare application
	// has an action associated with it:
	//Run: func(cmd *cobra.Command, args []string) { },

	Run: func(cmd *cobra.Command, args []string) {

		// Make sure there is a mode directive if we are not going directly

		// If we find something in the configuration then we can redirect to the application mode required.
		StroomMode := strings.ToLower(viper.GetString("Mode"))
		switch StroomMode {
		case "auditd":
			auditdCmd.Run(cmd, args)
		case "info":
			infoCmd.Run(cmd, args)
		default:
			fmt.Println("Unknown Mode found in configuration file", StroomMode)
			fmt.Println("Please Select a mode through either command line or Mode in config file")
			os.Exit(0)
		}

		//rootCmd.Run(cmd, args)
		//Run(cmd, args)
	},
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	cobra.CheckErr(rootCmd.Execute())
}

func init() {
	cobra.OnInitialize(initConfig)

	// Here you will define your flags and configuration settings.
	// Cobra supports persistent flags, which, if defined here,
	// will be global for your application.

	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is /etc/stroomc/stroomc.toml")

	// Cobra also supports local flags, which will only run
	// when this action is called directly.
	rootCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")

}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := homedir.Dir()
		cobra.CheckErr(err)

		// Search config in home directory with name ".stroomc" (without extension).
		viper.AddConfigPath(home)
		viper.SetConfigType("yaml")
		viper.AddConfigPath("/etc/stroomc/") // path to look for the config file in
		viper.AddConfigPath(".")             // optionally look for config in the working directory
		viper.SetConfigName("stroomc")
	}

	// TODO: Look up how this works and implement it in the application
	//viper.AutomaticEnv() // read in environment variables that match

	// If a config file is found, read it in. (Default - Maybe remove or adjust this code)
	if err := viper.ReadInConfig(); err == nil {
		log.Debug("Using config file:", viper.ConfigFileUsed())
	}

	// This is to capture errors reading in the configuration file that are not seen in the command above
	err2 := viper.ReadInConfig()
	if err2 != nil {
		fmt.Println("Error readinng configuration file")
		fmt.Println(err2.Error())
		os.Exit(0)
	}

	// Check we have a value for the config file,  if not then we exit with a message.
	if viper.ConfigFileUsed() == "" {
		log.Error("Could not find the config file,  please ensure a config file named stroomc.toml is in the /etc/stroomc or the program directory")
		os.Exit(1)
	}

	// Move to a debug directive
	log.Debug("Config File:", viper.ConfigFileUsed())

	// Set the log levels here .. default to nothing if the config is not sane.
	// Seems to work best here as it is the point we have initialised the config file
	utils.SetUpLogging()
}
