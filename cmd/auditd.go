/*
Copyright © 2021 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/attunga/stroomc/auditd"
	"gitlab.com/attunga/stroomc/settings"
	"gitlab.com/attunga/stroomc/utils"
)

// auditdCmd represents the auditd command
var auditdCmd = &cobra.Command{
	Use:   "auditd",
	Short: "A brief description of your command",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Run: func(cmd *cobra.Command, args []string) {
		log.Debug("auditd called")

		// Do a range or sanity checks before starting
		// These checks will log and exit fatal should they fail.
		// TODO:  Maybe move some of these to the settings section
		utils.DoAuditDSanityChecks()

		// Get the list of Proxies for the future Sender Process.
		// er
		proxies := settings.Proxies{}.LoadProxiesFromConfig()

		// Now run the main AuditD Processing Process
		auditd.ProcessAuditd(&proxies)
	},
}

func init() {
	rootCmd.AddCommand(auditdCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// auditdCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// auditdCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
