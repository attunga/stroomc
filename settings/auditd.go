/*
Copyright © 2021 Lindsay Steele <lgsteele@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package settings

import (
	"bufio"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"gitlab.com/attunga/stroomc/utils"
	"os"
	"strings"
)

// The main reason for this is to be able to do sanity checks once when using a configuration setting rather than
// doing them inline and having to do constant checks then.

type AuditdSettings struct {
	OperationMode        string
	QueueDirectory       string
	TempPath             string
	NumberOfFilesToQueue string
	MaximumQueueSize     string
	AuditLogFile         string
	CheckpointFile       string
	AuditDProcessor      string
	NeedNodeNames        bool
}

func (settings AuditdSettings) LoadSettings() AuditdSettings {

	// TODO: Should do some sanity checks here .... and set defaults should they be needed.
	settings.OperationMode = viper.GetString("OperationMode")
	settings.QueueDirectory = viper.GetString("QueueDirectory") //TODO: Need to make sure this directory does not have a trailing slash .. in case someone puts someething in the config.
	settings.TempPath = getTempPath()                           //TODO:  This could come from settings or just be found on the system
	settings.NumberOfFilesToQueue = viper.GetString("NumberOfFilesToQueue")
	settings.MaximumQueueSize = viper.GetString("MaximumQueueSize")
	settings.AuditLogFile = viper.GetString("AuditLogFile")
	settings.CheckpointFile = viper.GetString("CheckpointFile") // TODO: Send this out to a function that validates the path exists
	settings.AuditDProcessor = getAuditDProcessor()
	settings.NeedNodeNames = getNeedNodeNames(settings.AuditLogFile)

	return settings
}

func getTempPath() string {

	// Work out what our default is
	viper.SetDefault("PreferredTempFilePath", "/var/tmp")
	preferredTempPath := viper.GetString("PreferredTempPath")

	usedTempLocation := ""
	switch true {
	case utils.DirectoryExists(preferredTempPath):
		usedTempLocation = preferredTempPath
	case utils.DirectoryExists("/tmp"):
		usedTempLocation = "/tmp"
	case utils.DirectoryExists("/var/tmp"): // This is reserved for other known locations.
		usedTempLocation = "var/tmp"
	default:
		log.Fatal("Could not find a suitable Temp directory location")
	}

	log.Debug("Using Temp Directory: ", usedTempLocation)

	// Return First Temp File location that we have found
	return usedTempLocation

}

func getAuditDProcessor() string {

	// Work out what our default is
	viper.SetDefault("AuditDPreferredProcessor", "ausearch")
	auditdPreferredProcessorMode := viper.GetString("AuditDPreferredProcessor")

	log.Debug("Using Audit Preferred Processor: ", auditdPreferredProcessorMode)

	// I tried to find the ausearch binary though normal means of using exec.lookPath and which but ausearch is never
	// found.   I am not sure if this is a bug or something unique to do with

	// We will use a case statement instead to look in a range of common locations.
	// First we will use the provided location in the config file but then look in a number of other locations.
	// TODO: Maybe research while lookpath is not finding the ausearch binary.
	//

	ausearchLocation := ""

	switch true {
	case utils.FileExistsSimple("/sbin/ausearch"):
		ausearchLocation = "/sbin/ausearch"
	case utils.FileExistsSimple("/usr/sbin/ausearch"):
		ausearchLocation = "/usr/sbin/ausearch"
	case utils.FileExistsSimple("/usr/sbin/ausearch"): // This is reserved for other known locations.
		ausearchLocation = "/usr/sbin/ausssearch"
	default:
		log.Fatal("Could not find a suitable ausearch binary")
	}

	log.Debug("Using Ausearch location as: ", ausearchLocation)

	// Return the binary location for use in settings.
	return ausearchLocation
}

// This works out whether we need to insert node names.
// It basically scans a small segment of text looking to see if it starts with "type="
func getNeedNodeNames(auditFile string) bool {

	// Could go out to OS .. or we just open the file and have a look
	// head -2 ${_tf} | egrep '^type=' > /dev/null
	file, err := os.Open(auditFile)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	// optionally, resize scanner's capacity for lines over 64K, see next example
	const maxCapacity = 64000 // your required line length
	buf := make([]byte, maxCapacity)
	scanner.Buffer(buf, maxCapacity)

	foundTypeEquals := false
	loops := 0
	for scanner.Scan() {
		log.Trace("Checking Line Text for Type=: ", scanner.Text())

		if strings.HasPrefix(strings.ToLower(scanner.Text()), "type=") {
			foundTypeEquals = true
			log.Trace("Found a match on line ", scanner.Text())
		}
		// Just look at the first 5 lines.
		if loops > 4 {
			break
		}
		loops++
	}

	if err := scanner.Err(); err != nil {
		log.Fatal("Error Openings Audit Log File check for type=", err.Error())
	}

	// If we have found the text then we don't need to add the type,  so reverse the logic
	return !foundTypeEquals
}
