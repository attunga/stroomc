/*
Copyright © 2021 Lindsay Steele <lgsteele@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

package settings

import (
	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"net"
	"os"
	"os/exec"
	"strings"
	"time"
)

var Version = "Use LDFLAGS to Set"
var BuildTime = "Use LDFLAGS to Set"

type FeedInfo struct {
	Hostname       string //MyHost
	IpAddress      string //MyIPAddress
	TimeZone       string
	FeedName       string
	Compression    string
	SystemType     string
	Environment    string // Production, QualityAssurance or Development
	MySecurityZone string
	StroomcVersion string
	FeedVersion    string
}

func (feedInfo FeedInfo) LoadFeedInfo() FeedInfo {

	var err error
	feedInfo.Hostname, err = os.Hostname()
	if err != nil {
		feedInfo.Hostname = "Error Getting Hostname"
		log.Error("Error getting hostname: ", err.Error())
	}

	feedInfo.TimeZone = getTimeZone()
	// Could try to see if this comes back bland and then look up an os environment variable instead.
	feedInfo.IpAddress = getOutboundIP().String()

	viper.SetDefault("Compression", "GZIP")
	feedInfo.Compression = viper.GetString("Compression")

	viper.SetDefault("SystemType", "LinuxServer")
	feedInfo.SystemType = viper.GetString("SystemType")

	viper.SetDefault("Environment", "Production")
	feedInfo.Environment = viper.GetString("Environment")

	viper.SetDefault("MySecurityZone", "none")
	feedInfo.MySecurityZone = viper.GetString("MySecurityZone")

	viper.SetDefault("FeedName", "LINUX-AUDITD-AUSEARCH-V3-EVENTS")
	feedInfo.FeedName = viper.GetString("FeedName")

	feedInfo.StroomcVersion = Version

	// This Feedinfo is a mix of Kernel, AuditD version and local program version.
	feedInfo.FeedVersion = getFeedVersion()

	return feedInfo
}

// Specific Feedversion for Redhat Only
// TODO: Put in options allow this to work with Ubunutu or other OS's
func getFeedVersion() string {

	// Get the application version
	feedVersion := "Stroom Collector: " + Version

	// Get the Kernel Version
	cmd := exec.Command("uname", "-r")
	out, err := cmd.CombinedOutput()
	if err != nil {
		// Meh if it fails .. we don't care
		feedVersion = feedVersion + " : Unknown Kernel Version "
	}
	feedVersion = feedVersion + " : Kernel Version: " + strings.TrimSuffix(string(out), "\n")

	// Get the AuditD version
	cmdAudit := exec.Command("rpm", "-qa", "audit")
	out2, err2 := cmdAudit.CombinedOutput()
	if err2 != nil {
		// Meh if it fails .. we don't care
		feedVersion = feedVersion + " : Unknown Audit Version "
	}
	feedVersion = feedVersion + " : Audit Version: " + string(out2)

	return strings.TrimSuffix(feedVersion, "\n")
}

// Get preferred outbound ip of this machine
func getOutboundIP() net.IP {
	conn, err := net.Dial("udp", "8.8.8.8:80")
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()

	localAddr := conn.LocalAddr().(*net.UDPAddr)
	return localAddr.IP
}

func getTimeZone() string {
	t := time.Now()
	zone, _ := t.Zone()

	return zone
}
