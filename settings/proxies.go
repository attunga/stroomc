/*
Copyright © 2021 Lindsay Steele <lgsteele@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

package settings

import (
	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"math/rand"
	"time"
)

// Proxies are loaded early and there is only one instance - This allows them to be potentially used by
// different threads.
// Remote chance,  but it might also allow statistics to be pulled as it is running, maybe through a prometheus like
// interface.

type proxy struct {
	PreferenceOrder int // Also works as a kind of ID when trying to find this proxy.
	ProxyURL        string
	LastUsed        time.Time // This should be a fairly accurate time stamp on when it was last used.
	TimesUsed       int64
	TimesFailed     int64
}

type Proxies struct {
	AccessOrder                string
	AllProxies                 []proxy
	WorkingProxies             []proxy
	PreferredProxy             proxy
	LastUsedProxy              int // This is the order number of the last proxy used.
	NumberOfSucessful          int
	NumberOfFailed             int
	LogProcessingFrequency     int
	IgnoreTLSCertificateErrors bool
}

// Methods for Proxies

// LoadProxiesFromConfig Load Proxies from Config settings
func (proxies Proxies) LoadProxiesFromConfig() Proxies {

	// TODO: Fill initial struct with default values

	// Set the default for the proxy mode in case the config has not been set correctly
	viper.SetDefault("StroomProxyAccessOrder", "Preferred")
	proxies.AccessOrder = viper.GetString("StroomProxyAccessOrder")

	listOfProxies := viper.GetStringSlice("StroomProxies")
	if len(listOfProxies) == 0 {
		log.Fatal("No Proxies can be found in the configuration file,  please check Configuration and set StroomProxies setting")
	}

	for i, proxyURL := range listOfProxies {
		foundProxy := proxy{
			PreferenceOrder: i,
			ProxyURL:        proxyURL,
			//LastUsed: nil,
			TimesUsed:   0,
			TimesFailed: 0,
		}

		proxies.AllProxies = append(proxies.AllProxies, foundProxy)

		// Add the first Proxy as the first roxy
		if i == 0 {
			proxies.PreferredProxy = foundProxy
			log.Debug("Added preferred Proxy: ", foundProxy.ProxyURL)
		}
		log.Debug("Adding Proxy ", i, " With URL: ", foundProxy.ProxyURL)
	}

	// Set p the Log Processing Interval
	viper.SetDefault("LogProcessingFrequency", 10)
	minutesToWait := viper.GetInt("LogProcessingFrequency")
	if minutesToWait < 1 {
		minutesToWait = 10
		log.Error("Error getting value of Log Processing Frequency,  using a default of 10 minutes")
	}
	proxies.LogProcessingFrequency = minutesToWait
	log.Debug("Setting Processing Delay Interval: ", minutesToWait)

	// Set whether we ignore certificate errors on the proxy or not.
	viper.SetDefault("LogProcessingFrequency", "True")
	proxies.IgnoreTLSCertificateErrors = viper.GetBool("IgnoreTLSCertificateErrors")
	log.Debug("Setting Ignore Certificate Errors as: ", proxies.IgnoreTLSCertificateErrors)

	return proxies
}

// Set Mode

// Get a Random Known Working Proxy
func (proxies Proxies) GetRandomProxy() proxy {

	newProxy := proxies.AllProxies[rand.Intn(len(proxies.AllProxies))]

	log.Debug("Random Proxy: ", newProxy.ProxyURL)

	return newProxy
}

// Get a Random Proxy

// Get Next Proxy - Works on the last used Proxy and Adds the Next - For load Balance

// Get Random Proxy - Gets a Random Proxy from the know working Proxies

// Increment the Proxies successful Count
// AddSuccessful

// Increment the Proxies failed Could
// AddFailed

// Number of Working Proxies

// Number of Failed Proxies

// Get a range of Proxy statistics on this list of Proxies (Might need some way to make this loaded early
// or set up a singleton patern that can be called from elsehwere.
// Can be used for Monitoring as well for tooo many failed proxy updates .. something like proxies failed last 10 minutes
// Get Statistics  ... through port

// LoadProxiesFromConfigQuick This is a Function that is quickly used to get a basic Proxy settings config on shutdown.
// It quickly just sets the Access Order to preferred and then sets the first Proxy.
func (proxies Proxies) LoadProxiesFromConfigQuick() Proxies {

	proxies.AccessOrder = "Preferred"

	listOfProxies := viper.GetStringSlice("StroomProxies")

	for i, proxyURL := range listOfProxies {
		foundProxy := proxy{
			PreferenceOrder: i,
			ProxyURL:        proxyURL,
			//LastUsed: nil,
			TimesUsed:   0,
			TimesFailed: 0,
		}

		proxies.AllProxies = append(proxies.AllProxies, foundProxy)

		// Add the first Proxy as the first roxy
		if i == 0 {
			proxies.PreferredProxy = foundProxy
			log.Debug("Added preferred Proxy: ", foundProxy.ProxyURL)
		}
		log.Debug("Adding Proxy ", i, " With URL: ", foundProxy.ProxyURL)
	}

	// Set the first Proxy loaded from Config as the preferred Proxy
	return proxies
}
