Name:           stroomc
Version:        0.2.0
Release:        1%{?dist}
Summary:        Stroom Collector Agent Test Build

License:        GPLv3
Source0:        %{name}-%{version}.tar.gz
# BuildArch:	noarch
ExclusiveOS: 	Linux

Requires: audit-libs > 2.4.0 audit > 2.4.0 

BuildRequires:  golang >= 1.6.0

%if 0%{?el7}
BuildRequires:  systemd
%else
BuildRequires:  systemd-rpm-macros
%endif

Provides:       %{name} = %{version}

%description
A Stroom Collector Agent - currently does auditd on Red Hat based systems and derivatives

%global debug_package %{nil}

%prep
%autosetup

%build
go build -v -o %{name} -ldflags="-X 'gitlab.com/attunga/stroomc/settings.Version=%{version}' -X 'gitlab.com/attunga/stroomc/settings.BuildTime=$(date)'"

%install
install -Dpm 0750 %{name} %{buildroot}%{_bindir}/%{name}
install -Dpm 0640 stroomc.yml %{buildroot}%{_sysconfdir}/%{name}/stroomc.yml
install -Dm 0644 build/%{name}.service %{buildroot}%{_unitdir}/%{name}.service
install -Dm 0644 build/%{name}.rotate %{buildroot}/etc/logrotate.d/%{name}
install -dm 0750 %{buildroot}/var/log/stroomc
install -dm 0750 %{buildroot}/var/stroomc/queue

%check
# go test should be here... :)

%post
%systemd_post %{name}.service

%preun
%systemd_preun %{name}.service

%files
%dir %{_sysconfdir}/%{name}
%{_bindir}/%{name}
%{_unitdir}/%{name}.service
%config(noreplace) %{_sysconfdir}/%{name}/stroomc.yml
/etc/logrotate.d/%{name}
%dir /var/log/stroomc
%dir /var/stroomc/queue

%changelog
* Sun Aug 14 2021 Lindsay Steele 0.2.0
- Initial Test Build

