/*
Copyright © 2021 Lindsay Steele <lgsteele@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package auditd

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/attunga/stroomc/collector"
	"gitlab.com/attunga/stroomc/sender"
	"gitlab.com/attunga/stroomc/settings"
	"os"
	"os/signal"
	"syscall"
	"time"
)

var feedInfo settings.FeedInfo

func ProcessAuditd(proxySettings *settings.Proxies) {

	// settings where it just loads the info into Struct
	auditdSettings := settings.AuditdSettings{}.LoadSettings()

	// Initializes the Feed info that Contains information needed to post the feed.
	feedInfo = settings.FeedInfo{}.LoadFeedInfo()

	// Just a bit of debug info we will use in this mode.
	log.Debug("Hostname:", feedInfo.Hostname)
	log.Debug("IP:", feedInfo.IpAddress)
	log.Debug("TimeZone:", feedInfo.TimeZone)
	log.Debug("Version: ", feedInfo.StroomcVersion)
	log.Debug("BuildTime: ", settings.BuildTime)

	// The idea of this next section of code is to allow a cleanup on Control-C or program exit
	// setup signal catching
	sigs := make(chan os.Signal, 1)

	// catch all signals since not explicitly listing
	// TODO: Need to control the signals sent here .. it sometimes gets signals that do not mean close.
	//signal.Notify(sigs)
	signal.Notify(sigs, syscall.SIGQUIT, syscall.SIGTERM, syscall.SIGINT)

	// Method invoked upon seeing signal to exit
	// Then runs the cleanup method.
	go func() {
		s := <-sigs
		log.Info("RECEIVED SIGNAL: %s", s)
		AppCleanup()
		os.Exit(0)
	}()

	// Do the logs processing here  .. serially for now but go threads in future.
	for {
		log.Debug("Starting Processing Audit log filese ...")

		// TODO: This might be refactored to a seperate go thread in future,  maybe several threads.
		// Collect the audit log files and place them in the queue directory.
		numberOfFilesQueued, err := collector.ProcessAuditDLogs(&auditdSettings)
		if err != nil {
			log.Error("Error processing audit files: ", err.Error())
		} else {
			log.Info("Queued ", numberOfFilesQueued, " audit files for sending")
		}

		// Here we would most likely go out to a method
		// We go out to the sender method to send files from the configured directory
		// This might also be another process that is running in another thread seperate to this.
		// Keep it simple for now though ....
		numberOfFilesSet := sender.Processfiles(proxySettings, &feedInfo)
		log.Info("Sent ", numberOfFilesSet, " files to proxy")

		// Wait a defined number of minutes before doing the processing again
		// Randomness is not required as this will start when the system starts and not via a predefined cron.
		Nsecs := proxySettings.LogProcessingFrequency
		log.Debugf("About to sleep %d minutes before looping again: ", Nsecs)
		time.Sleep(time.Minute * time.Duration(Nsecs))
	}
}

// Do a final cleanup before clssing the application.
func AppCleanup() {
	log.Info("CLEANUP APP BEFORE EXIT!!!")
	// Try to send any files sitting in the queue before the program exits

	proxies := settings.Proxies{}
	proxies.LoadProxiesFromConfig()

	numberOfFilesSet := sender.Processfiles(&proxies, &feedInfo)
	log.Debug("Sent ", numberOfFilesSet, " files to proxy on exit")
}
