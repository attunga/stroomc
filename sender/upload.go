/*
Copyright © 2021 Lindsay Steele <lgsteele@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package sender

import (
	"crypto/tls"
	log "github.com/sirupsen/logrus"
	"io/ioutil"
	"net"
	"net/http"
	"os"
	"time"
)

// TODO: Set this as a function that can run in paralell as a thread
func PostToHTTPS(url string, filename string) (int, error) {

	sendFiles := true
	// TODO: Implement a debug mode to stop uploading files
	if !sendFiles {
		return 200, nil
	}

	// TODO: Verify the file exists

	// Set up a number of transport options
	tr := &http.Transport{
		MaxIdleConns:       10,
		DisableCompression: false,
		TLSClientConfig:    &tls.Config{InsecureSkipVerify: proxySettings.IgnoreTLSCertificateErrors}, // Set whether we ignore certificate errors

		Dial: (&net.Dialer{ // TODO: Change this deprecated setting here to DialContext - which on research seems very complex
			Timeout: 5 * time.Second, // TODO: Extract this out to settings - half of main timeout.
		}).Dial,
		TLSHandshakeTimeout:   10 * time.Second, // TODO: Extract to settings .. half http connect timeout time
		IdleConnTimeout:       30 * time.Second,
		ExpectContinueTimeout: 1 * time.Second,
		ResponseHeaderTimeout: 10 * time.Second,
	}

	// Create a client to allow HTTP client options if required.
	client := &http.Client{
		// client Options here if required
		// CheckRedirect: redirectPolicyFunc,
		Timeout:   time.Second * 10,
		Transport: tr,
	}

	data, err := os.Open(filename)
	//data, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Warn("Error opening filename:", err.Error())
		return 0, err
	}
	defer data.Close()

	log.Debug("Opened file with name:", data.Name())

	// Data needs to be an IO reader from what I can tell
	req, err := http.NewRequest("POST", url, data)
	if err != nil {
		log.Warn("Error trying to set up request:", err.Error())
		return 0, err
	}

	// TODO: Go over these Settings and MAke sure that they are Accurate and what is EXPECTED
	// Add in our headers here
	req.Header.Add("Feed", feedInfo.FeedName)     // unique Feed name from configuration
	req.Header.Add("System", feedInfo.SystemType) // Server or workstation
	req.Header.Add("Environment", feedInfo.Environment)
	req.Header.Add("Version", feedInfo.FeedVersion)
	req.Header.Add("MyHost", feedInfo.Hostname)
	req.Header.Add("MyIPaddress", feedInfo.IpAddress)
	req.Header.Add("MySecurityDomain", feedInfo.MySecurityZone)

	//Future Authentication??
	//req.SetBasicAuth("api", api_key)

	// There is where we try to do the actual posting ...
	resp, err := client.Do(req)
	if err != nil {
		log.Warn("Error trying to post file :", err.Error())
		return 0, err
	}
	defer resp.Body.Close()

	content, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}

	// Set the response output to Debug logs
	// TODO: Play with the response that comes back to get something useful for the debug logs.
	log.Debug("Posting Response: ", string(content))

	// Return nil if we have encountered no errors and things appear to be successful.
	return resp.StatusCode, nil
}
