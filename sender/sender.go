/*
Copyright © 2021 Lindsay Steele <lgsteele@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package sender

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"gitlab.com/attunga/stroomc/settings"
	"gitlab.com/attunga/stroomc/utils"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
)

// Make the Feed Info avaiable to the Sender Package - rather than cascading functions.
var feedInfo *settings.FeedInfo
var proxySettings *settings.Proxies

func Processfiles(sentProxySettings *settings.Proxies, sentFeedInfo *settings.FeedInfo) int {

	// Make these global for this package to save cascading settings
	feedInfo = sentFeedInfo
	proxySettings = sentProxySettings

	queueDirectory := viper.GetString("QueueDirectory")

	// check that the queue directory exists
	if _, err := os.Stat(queueDirectory); os.IsNotExist(err) {
		log.Warn("Error: ", err.Error())
		log.Fatal("Could not find the Queue directory ", queueDirectory)
	} else {
		log.Debug("Found Queue directory ", filepath.Clean(queueDirectory), " sucessfully")
	}

	// get a listing of files in the queue directory with .ready on them.
	files, err := ioutil.ReadDir(queueDirectory)
	if err != nil {
		log.Fatal(err)
	}

	var filesToSend []string

	for _, file := range files {
		// Process Files but ignore directories
		if !file.IsDir() {
			log.Debug(file.Name(), file.IsDir())

			// check if a ready file exists
			fullFilePath := filepath.Clean(queueDirectory) + "/" + file.Name()
			log.Debug("looking for: " + fullFilePath)
			if utils.FileExistsSimple(fullFilePath + ".ready") {
				log.Debug("We found a ready file .. adding file to queue to be sent")
				filesToSend = append(filesToSend, fullFilePath)
			} else {
				log.Debug("Cannot find: " + fullFilePath + ".ready")
			}
		}
	}

	log.Debug("Files Ready to Send: ", len(filesToSend))
	numberOfFilesSent := sendFilesToStroom(filesToSend)

	return numberOfFilesSent
}

// TODO: Put some logic in here to do multiple uploads to various proxies
// if we have more than one file and more than one proxy .. could also be round robin.
func sendFilesToStroom(filesToSend []string) int {

	numberOfFilesSent := 0

	for _, filename := range filesToSend {

		log.Debug("Processing File for upload: ", filename)
		// Read in the File

		// Upload the File
		//  Get the proxy we are going to use,   this could be a memory thing
		// remote function checks we have a workable proxy
		// HArd Coded for now.
		// TODO: Here we would make a decision on the mode we are  using,  here
		// the example uses prefferred proxy but we could use round robin etc .. and also redcheck proxies
		// if there is an error found.
		stroomProxyUrl := ""
		switch strings.ToLower(proxySettings.AccessOrder) {
		case "preferred":
			log.Trace("Using Preferred Proxy Mode")
			stroomProxyUrl = proxySettings.PreferredProxy.ProxyURL
		case "roundrobin":
			fmt.Println("Using Round Robin Method")
		case "random":
			stroomProxyUrl = proxySettings.GetRandomProxy().ProxyURL
			log.Debug("Using Random Proxy Method")
		default:
			log.Error("Can't Determine the proxy mode,  we will just use Preferred - Please check Config File")
			stroomProxyUrl = proxySettings.PreferredProxy.ProxyURL
		}

		// TODO: Maybe stop and log if there are no proxies available

		response, err := PostToHTTPS(stroomProxyUrl, filename)
		if err != nil {
			//TODO:  Log the error ... work out whether we stop or not
			log.Debug("Failed Posting to Stroom:", err.Error())
		}

		log.Info("File:", filename, " uploaded with response code:", response)

		// Look for a Response Code 200 .. and then remove the files and ready now we are done.
		if response == 200 {
			numberOfFilesSent++
			// Remove the ready files now that it is sucessfully updated
			err := os.Remove(filename + ".ready")
			if err != nil {
				log.Error("Error removing ready file: ", filename+".ready")
			}

			// Remove the gzip files now that it is sucessfully updated
			err2 := os.Remove(filename)
			if err2 != nil {
				log.Error("Error removing ready file: ", filename+".ready")
			}
		}

	}
	return numberOfFilesSent
}
