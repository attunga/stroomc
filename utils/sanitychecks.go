package utils

import (
	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"os"
	"os/user"
	"path/filepath"
)

func DoAuditDSanityChecks() {

	// For Audit we need to ensure we are running as root
	checkRoot()

	//Do some Sanity Checks

	//Make sure the Audit Log File exists.
	// Will exit fatal if it does not exist or is not accessible.
	checkLogFileExists()

	//Check that the path for the Checkpoint file exists.  Only path should exist, not the checkpoint file itself.
	// Will exit fatal if the directory does not exist or is not accessible.
	checkCheckpointDirectoryExists()

	// Check that the queue directory exists
	// Will exit fatal if the directory does not exist or is not accessible.
	checkQueueDirectoryExists()

}

func checkRoot() {

	// This only works in Golang 1.16 or later
	currentUser, err := user.Current()
	if err != nil {
		log.Fatalf("[isRoot] Unable to get current user: %s", err)
		log.Fatalf("Make sure you are using golang 1.16 or greater")
		os.Exit(1)
	}
	if currentUser.Username == "root" {
		log.Debug("You are root!!")
	} else {
		log.Fatal("Please run with root level privileges")
		//os.Exit(1)
	}

}

func checkLogFileExists() {

	viper.SetDefault("AuditLogFile", "/var/log/audit.log")
	logfile := viper.GetString("AuditLogFile")

	fileExists, err := FileExistsDebug(logfile)

	if fileExists {
		log.Debug("Audit Log File Found: ", logfile)
	} else {
		log.Fatal("Could not find log file: ", logfile, " with error", err.Error())
	}

}

func checkCheckpointDirectoryExists() {

	viper.SetDefault("CheckpointFile", "/var/tmp/checkpoint.txt")
	checkpointfile := viper.GetString("CheckpointFile")

	filePath := filepath.Dir(checkpointfile)

	log.Debug("Using File Path: ", filePath)

	fileExists, err := DirectoryExistsDebug(filePath)

	if fileExists {
		log.Debug("Checkpoint file path Exists: ", filePath)
	} else {
		log.Fatal("Could not find Checkpoint file path: ", filePath, " with error", err.Error())
	}

}

func checkQueueDirectoryExists() {

	viper.SetDefault("QueueDirectory", "/var/stroomc/queue/")
	queueDirectory := viper.GetString("QueueDirectory")

	filePath := filepath.Dir(queueDirectory)

	log.Debug("Using Queue Path: ", filePath)

	fileExists, err := DirectoryExistsDebug(filePath)

	if fileExists {
		log.Debug("Queue Path Exists: ", filePath)
	} else {
		log.Fatal("Could not find queue path: ", filePath, " with error", err.Error())
	}

}
