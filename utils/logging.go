/*
Copyright © 2021 Lindsay Steele <lgsteele@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package utils

import (
	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"os"
	"strings"
)

func SetUpLogging() {

	// Set up the logging here ..
	viper.ReadInConfig()
	LogLevel := strings.ToLower(viper.GetString("LogLevel"))
	//fmt.Println("LogLevel Found: ", LogLevel)
	switch LogLevel {
	case "tracelevel", "trace", "6":
		log.SetLevel(log.TraceLevel)
	case "debuglevel", "debug", "5":
		log.SetLevel(log.DebugLevel)
	case "infolevel", "info", "4":
		log.SetLevel(log.InfoLevel)
	case "warnlevel", "warn", "3":
		log.SetLevel(log.WarnLevel)
	case "errorlevel", "error", "2":
		log.SetLevel(log.ErrorLevel)
	case "fatallevel", "fatal", "1":
		log.SetLevel(log.FatalLevel)
	case "paniclevel", "panic", "0":
		log.SetLevel(log.PanicLevel)
	default:
		log.SetLevel(log.WarnLevel)
		log.Warn("Could not find a valid setting for a level in config file, setting Logging Level to Warn")
	}

	log.SetOutput(os.Stdout)

	// Set the custom formatter to give the date in a required format as part of the logs.
	customFormatter := new(log.TextFormatter)
	customFormatter.TimestampFormat = "2006-01-02 15:04:05"
	log.SetFormatter(customFormatter)
	customFormatter.FullTimestamp = true

	logType := strings.ToLower(viper.GetString("LogType"))
	switch logType {
	case "file":
		setupFileLogging()
		// Debug message in method for brevity
	case "stdout":
		log.SetOutput(os.Stdout)
		log.Debug("Logging set to StdOut")
	case "stderr":
		log.SetOutput(os.Stderr)
		log.Debug("Logging set to StdErr")
	default:
		log.SetOutput(os.Stdout)
		log.Error("Error encountered reading LogType configuration setting - Logging set to StdOut")
	}
	log.Debug("Setting Logging level to: ", log.GetLevel())
}

func setupFileLogging() {
	logFileLocation := viper.GetString("LogFileLocation")
	// You could set this to any `io.Writer` such as a file
	file, err := os.OpenFile(logFileLocation, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)
	if err == nil {
		log.SetOutput(file)
	} else {
		log.Warn("Failed to log to file, using default stderr")
		log.Warn("Error Meessage:", err.Error())
		log.SetOutput(os.Stderr)
		return
	}
	log.Debug("Setting logging to: ", logFileLocation)
}
